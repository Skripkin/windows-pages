class Tabs {
  constructor() {
    this._tabs = document.querySelector(".tabs");

    if(!this._tabs) {
      return this.destructor();
    }

    this._addListeners();
  }

  destructor() {
    this._tabs = null;
  }

  _addListeners() {
    const buttons = this._tabs.querySelectorAll(".tabs__button");

    buttons.forEach((button, i) => {
      button.addEventListener("click", e => this._handleButtonClicked(e.target, i + 1));
    });
  }

  _handleButtonClicked(button, index) {
    const blocks = this._tabs.querySelectorAll(".tabs__info");
    const buttons = this._tabs.querySelectorAll(".tabs__button");

    buttons.forEach(item => {
      item.classList.remove("tabs__button--active");
    });

    button.classList.add("tabs__button--active");

    blocks.forEach(block => {
      if(block.classList.contains(`tabs__info-${index}`)) {
        block.classList.remove("tabs__info--hidden");
      } else {
        block.classList.add("tabs__info--hidden");
      }
    });
  }
}

export default new Tabs();