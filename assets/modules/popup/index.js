class Popup {
  constructor() {
    this._popup = document.querySelector(".popup");
    this._services = document.querySelectorAll(".service");

    if(!this._popup && !this._services) {
      return this.destructor();
    }

    this._addListeners();
  }

  destructor() {
    this._popup = null;
  }

  _addListeners() {
    const submitsButtons = document.querySelectorAll(".service__submit-button");
    const closeButton = this._popup.querySelector(".popup__close");

    submitsButtons.forEach(button => {
      button.addEventListener("click", e => {
        this._popup.classList.add("popup--opened");

        if(e.target.classList.contains("options__button")) {
          this._handleOptionsButtonClicked();
        }
      });
    });

    closeButton.addEventListener("click", () => {
      this._handlePopupClosed(this._popup);
    })
  }

  _handlePopupClosed(popup) {
    popup.classList.remove("popup--opened");

    if(document.querySelector(".popup__secondary-text")) {
      document.querySelector(".popup__secondary-text").remove();
    }
  }

  _handleOptionsButtonClicked() {
    const options = document.querySelectorAll(".option");
    const arr = {
      "Двухстворчатое окно": [],
      "Трехстворчатое окно": [],
      "Балконный блок": [],
    };
    let bool = false;

    options.forEach(option => {
      const optionInput = option.querySelector(".option__input");

      if(optionInput.checked) {
        const optionPrice = option.querySelector(".option__title").textContent;

        arr[option.getAttribute("option-type")].push(optionPrice);
        bool = true;
      }
    });

    if(bool) {
      this._handleTextAdded(arr);
    }
  }

  _handleTextAdded(arr) {
    const popupContainer = document.querySelector(".popup__flex-box");
    const block = document.createElement("div");

    block.classList.add("popup__secondary-text");
    
    Object.keys(arr).forEach(item => {
      const p = document.createElement("p");
      if(arr[item].length !== 0) {
        p.append(item + ": ");
      }
      arr[item].forEach(text => p.append(text.replace("от ", "") + ", "));
      block.append(p);
    })

    popupContainer.before(block);
  }
}

export default new Popup();
