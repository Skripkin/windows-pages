import { tns } from 'tiny-slider/src/tiny-slider';

class ProductSlider {
    constructor() {
        this._productSlider = document.querySelectorAll('.product-slider');
        
        if(!this._productSlider) {
            return this.destructor();
        }

        this._productSlider.forEach(productSlider => {
            const prevButton = productSlider.querySelector('.product-slider__prev-arrow');
            const nextButton = productSlider.querySelector('.product-slider__next-arrow');
            const slides = productSlider.querySelectorAll('.product-slider__slide');
            const container = productSlider.querySelector('.product-slider__slider');
            const navContainer = productSlider.querySelector('.product-slider__nav-container')
            
            if (slides.length > 1) {
                this._slider = tns({
                    container,
                    prevButton,
                    nextButton,
                    controlsText: [
                        '<span class="product-slider__arrow product-slider__prev-arrow"><svg xmlns="http://www.w3.org/2000/svg" width="80px" viewBox="0 0 56 33" fill="none"></path><path d="M38.4515 1.61115L54.7095 16.6367L38.5328 31.5931" stroke="#ffffff" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"></path></svg></span>',
                        '<span class="product-slider__arrow product-slider__next-arrow"><svg xmlns="http://www.w3.org/2000/svg" width="80px" viewBox="0 0 56 33" fill="none"></path><path d="M38.4515 1.61115L54.7095 16.6367L38.5328 31.5931" stroke="#ffffff" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"></path></svg></span>'
                    ],
                    rewind: true,
                    loop: true,
                    items: 1,
                    navContainer: navContainer,
                    swipeAngle: false,
                    speed: 400,
                    autoplay: true,
                    autoplayTimeout: 3000,
                    navAsThumbnails: true,
                    navPosition: 'bottom',
                    freezable: false,
                    // lazyload: true,
                });
            }
        });
    }
    
    destructor() {
        if(this._slider) {
            this._slider.destroy();
            this._slider = null;
        }
        
        this._productSlider = null;
    }
}

export default new ProductSlider();