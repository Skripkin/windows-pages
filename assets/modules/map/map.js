class Map {
    constructor() {
        this._mapContainer = document.getElementById("map");

        if(!this._mapContainer) {
            return this.destructor();
        }

        const myOptions = {
            center: new google.maps.LatLng(43.2566700, 76.9286100),
            zoom: 9,
            fullscreenControlOptions: {
                position: google.maps.ControlPosition.TOP_LEFT
            },
            mapTypeControl: true,
            mapTypeControlOptions: {
                position: google.maps.ControlPosition.BOTTOM_LEFT,
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
            },
        };

        const map = new google.maps.Map(this._mapContainer, myOptions);
    }

    destructor() {
        this._mapContainer = null;
    }
}

export default new Map();
