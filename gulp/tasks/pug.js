module.exports = function() {
  $.gulp.task("pug", function() {
    return $.gulp.src("assets/pages/*.pug")
      .pipe($.gp.pug({
          pretty: true
      }))
      .pipe($.gulp.dest("public"))
      .on("end", $.bs.reload);
  });
}